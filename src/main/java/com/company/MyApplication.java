package com.company;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {

    private ArrayList<User> Users = new ArrayList<>();
    private User signedUser, findingUser;

    private ArrayList<Admin> Admins = new ArrayList<>();

    private void addUser(User user) { Users.add(user); }

    private void addAdmin(Admin admin) { Admins.add(admin); }

    public void start() throws IOException {
        File file = new File("C:\\Users\\kuany\\IdeaProjects\\SocialApp\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNext()) {
            int a = fileScanner.nextInt();
            String b = fileScanner.next();
            String c = fileScanner.next();
            String d = fileScanner.next();
            String e = fileScanner.next();
            User user = new User(a, b, c, d, e);
            addUser(user);
        }
        File file1 = new File("C:\\Users\\kuany\\IdeaProjects\\SocialApp\\src\\com\\company\\adminlist.txt");
        Scanner fileScannner1 = new Scanner(file1);
        while (fileScannner1.hasNext()) {
            int a = fileScannner1.nextInt();
            String b = fileScannner1.next();
            String c = fileScannner1.next();
            String d = fileScannner1.next();
            String e = fileScannner1.next();
            Admin admin = new Admin(a, b, c, d, e);
            addAdmin(admin);
        }
        Scanner sc = new Scanner(System.in);
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Menu");
                System.out.println("2. Admin menu");
                System.out.println("3. Exit");
                int i = sc.nextInt();
                if (i == 1) {
                    menu();
                }
                else if(i == 3){
                    saveUserList();
                    break;
                }
                else {
                    admin_menu();
                }
            }
            else
            {
                System.out.println("1. See profile");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if(choice == 1)
                    Profile();
                else
                {
                    logOut();
                    saveUserList();
                    break;
                }
            }
        }
    }
    private void admin_menu() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("1. Login");
        System.out.println("2. Exit|Log out");
        int i = sc.nextInt();
        if(i == 1)
            auth();
    }

    private void auth() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Username: ");
        String pass = "";
        int i = 3;
        while (i != 0) {
            String a = sc.next();
            if (findAdmin(a)) {
                for (Admin admin : Admins)
                    if (a.equals(admin.getUsername())) {
                        pass = a;
                        break;
                    }
                if(!pass.equals(""))
                    break;
            } else {
                System.out.println("Wrong Username: ");
                i--;
            }
        }
        if(!(i == 0 && !pass.equals(""))) { }
        else {
            i = 3;
            while(i != 0) {
                String a = sc.next();
                if (a.equals(pass)) {
                    admin_Profile();
                    break;
                } else {
                    i--;
                }
            }
        }
    }

    private boolean findAdmin(String s) {
        for(Admin admin : Admins)
            if (s.equals(admin.getUsername()))
                return true;
        return false;
    }

    private void admin_Profile() throws IOException {
        Scanner sc = new Scanner(System.in);
        while(true) {
            int i = sc.nextInt();
            System.out.println("1. Show Users");
            System.out.println("2. Show User's Profile");
            System.out.println("3. Delete User");
            System.out.println("4. Log out");
            if(i == 1) {
                for(User user : Users) {
                    System.out.println(user.getId() + " " + user.getUsername() + " " + user.getName() + " " + user.getSurname() + "\n");
                }
            }
            if(i == 2) {
                System.out.println("Type User's Username: ");
                while(true)
                {
                    String j = sc.next();
                    if(j.equals("Back"))
                        break;
                    if(findUsername(j)) {
                        signedUser = returnUser(j);
                        Profile(i);
                    }
                    else {
                        System.out.println("Wrong Username: ");
                    }
                }
            }
            if(i == 3) {
                System.out.println("Type User's Username: ");
                while(true)
                {
                    String j = sc.next();
                    if(j.equals("Back"))
                        break;
                    if(findUsername(j)) {
                        Users.remove(returnUser(j));
                        break;
                    }
                    else {
                        System.out.println("Wrong Username: ");
                    }
                }
            }
            if(i == 4) { break; }
        }
    }

    private void menu() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Options :");
        System.out.println("1. Log in");
        System.out.println("2. Sign up");
        System.out.println("3. Go back");
        int a = sc.nextInt();
        if(a == 1) Login();
        if(a == 2) SignUp();
    }


    private void Login () throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Username : ");
        String s;
        s = sc.nextLine();
        while(!findUsername(s))
        {
            System.out.println("Wrong Username :");
            s = sc.nextLine();
        }
        System.out.println("Password :");
        String t;
        t = sc.nextLine();
        Password password = new Password(t);
        if(!password.checkPassword(s, Users)) {
            System.out.println("Wrong Password : ");
            int checkNum = 0;
            while(checkNum <= 2)
            {
                t = sc.nextLine();
                password.update(t);
                System.out.println("Wrong Password : ");
                if(password.checkPassword(s,Users)) {
                    break;
                }
                checkNum++;
            }
            if(checkNum > 2) {
                System.out.println("You have already spent 3 chances");
                menu();
            }
        }
        for(User user : Users)
            if(user.getUsername().equals(s))
                signedUser = user;
        Profile();
    }

    private boolean findUsername(String u) {
        for(User user : Users)
        {
            if(user.getUsername().equals(u))
            {
                return true;
            }
        }
        return false;
    }

    private User returnUser(String u) {
        for(User user : Users)
        {
            if(user.getUsername().equals(u))
            {
                return user;
            }
        }
        return null;
    }

    private void SignUp() throws IOException {
        Scanner sc = new Scanner(System.in);
        String name,surname,username,password;
        System.out.println("Your name :");
        name = sc.nextLine();
        System.out.println("Your surname :");
        surname = sc.nextLine();
        System.out.println("Your username :");
        username = sc.nextLine();
        if(findUsername(username))
        {
            while(true)
            {
                System.out.println("Find another one :");
                username = sc.nextLine();
                if(!findUsername(username))
                    break;
            }
        }
        System.out.println("Password :");
        System.out.println("It should contain at least upper-case and lower-case letters and digit, also minimal length is 8");
        password = sc.nextLine();
        Password p = new Password(password);
        if(!p.check())
        while(true)
        {
            password = sc.nextLine();
            p.update(password);
            if(p.check())
                break;
        }
        User n = new User(name, surname, username, password);
        signedUser = n;
        Users.add(n);
        Profile();
    }

    private void Profile(int i) throws IOException {
        while (true) {
            Scanner sc = new Scanner(System.in);
            if (signedUser != null) {
                System.out.println("You are signed in.");
                System.out.println("1. See name");
                System.out.println("2. See surname");
                System.out.println("3. See username");
                System.out.println("4. See password");
                System.out.println("5. Back");
                int j = sc.nextInt();
                if(j == 1) {System.out.println(signedUser.getName() + "\n");}
                if(j == 2) {System.out.println(signedUser.getSurname() + "\n");}
                if(j == 3) {System.out.println(signedUser.getUsername() + "\n");}
                if(j == 4) {System.out.println(signedUser.getPassword() + "\n");}
                if(j == 5) {signedUser = null; break;}
            }
        }
    }

    private void Profile() throws IOException {
        while (true) {
            Scanner sc = new Scanner(System.in);
            if (signedUser != null) {
                System.out.println("You are signed in.");
                System.out.println("1. See name");
                System.out.println("2. See surname");
                System.out.println("3. See username");
                System.out.println("4. See password");
                System.out.println("5. Go to menu");
                System.out.println("6. Log out");
                int choice = sc.nextInt();
                if(choice == 1) {System.out.println(signedUser.getName() + "\n");}
                if(choice == 2) {System.out.println(signedUser.getSurname() + "\n");}
                if(choice == 3) {System.out.println(signedUser.getUsername() + "\n");}
                if(choice == 4) {System.out.println(signedUser.getPassword() + "\n");}
                if(choice == 5) {break;}
                if(choice == 6) {logOut(); break;}
            }
        }
    }


    private void logOut() {
        signedUser = null;
    }
    private void saveUserList() throws IOException {
        String c = "";
        for(User user : Users){
            c += user.getId() + " " + user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPassword() + "\n";
        }
        Files.write(Paths.get("C:\\Users\\kuany\\IdeaProjects\\SocialApp\\src\\com\\company\\db.txt"), c.getBytes());
    }
}
