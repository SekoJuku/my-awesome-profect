package com.company;

import java.util.ArrayList;

public class Password {
    private String passwordStr;

    public Password(String s) { passwordStr = s; }

    public void update(String s) { passwordStr = s; }

    public String getPassword () { return passwordStr; }

    public boolean checkPassword(String u, ArrayList<User> Users)
    {
        for(User user : Users)
        {
            if(user.getUsername().equals(u))
            {
                if(user.getPassword().equals(passwordStr))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean check() {
        int digit = 0, lower = 0, upper = 0;
        for(int i = 0; i < passwordStr.length(); i++)
        {
            if(passwordStr.charAt(i) >= 'A' && passwordStr.charAt(i) <= 'Z')
                upper++;
            if(passwordStr.charAt(i) >= 'a' && passwordStr.charAt(i) <= 'z')
                lower++;
            if(passwordStr.charAt(i) >= '0' && passwordStr.charAt(i) <= '9')
                digit++;
        }
        if(digit > 0 && lower > 0 && upper > 0)
            return true;
        else
            return false;
    }
}
