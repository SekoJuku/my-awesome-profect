package com.company;

public class User {
    private static int id_gen = 1, mx = 1;
    protected String name,surname,username;
    protected Password password;
    protected int id;

    public User() {
        id = id_gen++;
        mx++;
    }

    public User (String Name, String Surname, String Username, String Password) {
        this();
        setName(Name);
        setSurname(Surname);
        setUsername(Username);
        setPassword(Password);
    }

    public User(int id, String Name, String Surname, String Username, String Password) {
        this.id = id;
        if(mx < id)
        {
            mx = id;
        }
        id_gen = mx;
        id_gen++;
        setName(Name);
        setSurname(Surname);
        setUsername(Username);
        setPassword(Password);
    }
    public void setName(String s) { name = s; }
    public void setSurname(String s) { surname = s; }
    public void setUsername(String s) { username = s; }
    public void setPassword(String s) { password.update(s); }

    public int getId() { return id; }
    public String getName() { return name; }
    public String getSurname() { return surname; }
    public String getUsername() { return username; }
    public String getPassword() { return password.getPassword(); }

}
